# Draw skeleton
 This module recieve video (.mp4) and keypoints data which is written in cocoformat (.json), to draw over-layed skeleton on video.


## API
 
| Route | Method | Query / Body | Description |
| --- | --- | --- | --- |
| /skeleton_video | POST | - | Post a video(.mp4 ) and keypoints data (.json written in coco format) to draw over-layed skeleton on video. |
| /skeleton_video | GET | fname | GET a video(.mp4 ) with over-layed skeleton. The original video name should be passed as query. |

### Example  
curl -X 'POST' \
  'http://{your-url}/skeleton_video/' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'file_video=@my-video.mp4;type=video/mp4' \
  -F 'file_coco=@my-keypoints.json;type=application/json'

curl -X 'GET' \
  'https://{your-url}/skeleton_video/?fname=my-video.mp4' \
  -H 'accept: application/json'

## Environment variables

| Variable | required | Description |
| --- | --- | --- |
| APP_PORT | false | The port to which the application listens to, default is set to 80 |


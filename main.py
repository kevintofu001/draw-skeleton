
#import yolo
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routes import keypoints


app = FastAPI()

#origins = [
#    "http://localhost",
#]
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "Hello World"}

app.include_router(keypoints.router)

if __name__ == "__main__":

    import os
    import uvicorn
    import argparse

    myport = os.getenv('APP_PORT', '80')
    print("myport", myport)

    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', '-G', type=int, default='-1', help='gpu number')
    parser.add_argument('--port', '-P', type=int, default=myport, help='port for http server')
    args = parser.parse_args()

    uvicorn.run('main:app', host="0.0.0.0", port=args.port)
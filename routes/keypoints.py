import os, sys

from fastapi import APIRouter, Request, File, UploadFile, File
from typing import List, Optional

from controllers.keypoints import*

router = APIRouter(prefix="")

@router.post('/skeleton_video/')
async def post_skeleton_video(file_video: UploadFile = File(...), \
                             file_coco: UploadFile = File(...)):
    return await post_skeleton_video_(file_video, file_coco)

@router.get('/skeleton_video/')
def get_skeleton_video(fname: Optional[str] = None):
    return get_skeleton_video_(fname)

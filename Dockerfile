FROM python:3.8.10-slim-buster

RUN apt-get -qq update && apt-get -qqy dist-upgrade
#RUN apt-get -y update && \
#    apt-get -y upgrade

RUN apt-get -y install libgl1-mesa-dev wget
RUN apt-get update && apt-get install -y libopencv-dev 

RUN pip config list
RUN pip --timeout=300 install --upgrade pip
RUN pip install --upgrade pypi

RUN pip install opencv-python
RUN pip install opencv-contrib-python

WORKDIR /myapp
COPY ./ ./

RUN pip install --upgrade pip
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

EXPOSE 80
CMD ["python", "./main.py"]
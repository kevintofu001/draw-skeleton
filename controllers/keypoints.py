
import os
import shutil
from fastapi.responses import FileResponse, JSONResponse
from fastapi import HTTPException

from controllers import draw
from logconf import mylogger

path_temp = './temp/'
if os.path.exists(path_temp) == False:
    os.makedirs(path_temp)

logger = mylogger(__name__)
print('__name__', __name__)


def save_file(temp_filename, file, ext):

    logger.debug("save_file")
    _file = file
    extension = _file.filename.split('.')[-1] in ext
    if not extension:
        temp = ""
        for i in ext:
            temp += i + ' '
        raise HTTPException(status_code=400, detail=f'The file-extention is NOT ${temp}')
    
    with open(temp_filename, 'wb') as local_temp_file:
        local_temp_file.write(_file.file.read())


def save_video(temp_filename, file):
    save_file(temp_filename, file, ['mp4', 'MP4'])

def save_json(temp_filename, file):
    save_file(temp_filename, file, ['json', 'JSON'])

async def post_skeleton_video_(file_video, file_coco):
    
    logger.debug("post_skeleton_video_")
    logger.info(f'{file_video.filename}, {file_video.content_type}')
    logger.info(f'{file_coco.filename}, {file_coco.content_type}')

    extension = file_video.filename.split('.')[-1] in ('mp4') 
    if not extension:
        raise HTTPException(status_code=400, detail="The file is NOT .mp4")

    temp_filename_video = path_temp + file_video.filename.split('.')[0] + '_temp.mp4'
    temp_filename_json = path_temp + file_coco.filename.split('.')[0] + '_temp.json'
    export_filename = path_temp + file_video.filename.split('.')[0] + '_key.mp4'
    export_filename2 = path_temp + 'temp_key.mp4'
    save_video(temp_filename_video, file_video)
    save_json(temp_filename_json, file_coco)

    
    try:
        draw.draw_keypoint2video(export_filename, temp_filename_video, temp_filename_json)
        if os.path.exists(export_filename):
            shutil.copy2(export_filename, export_filename2)

        os.remove(temp_filename_video)
        os.remove(temp_filename_json)

    except:
        raise HTTPException(status_code=503, detail="Internal Error") 

    return {'status': 'OK'}


def get_skeleton_video_(fname):
    
    logger.debug("get_skeleton_video_")
    if fname is None:
        path_export = path_temp + 'temp_key.mp4'
    else:
        path_export = path_temp + fname.split('.')[0] + '_key.mp4'
    logger.info(f'export: {path_export}')
    
    if os.path.exists(path_export) == True:
        return FileResponse(path_export, filename=path_export)
    else:
        raise HTTPException(status_code=500, detail='Error')
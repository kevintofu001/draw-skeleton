
import os
import numpy as np
import cv2
from pycocotools.coco import COCO
import pylab as plt
from skimage.draw import line

def minmax(value, _min, _max):
    value = min(value, _max)
    value = max(value, _min)
    return value

def draw_keypoint2img(img, labels, pairs, color = [255, 0, 0], th=0.5):

    ret = np.copy(img)
    print(ret.shape)

    for label in labels:
        keypoints = np.array(label['keypoints'])
        keypoints = keypoints.reshape((keypoints.shape[0]//3, 3))
        scores = np.array(label['keyscore'])
        for pair in pairs:
            score1 = scores[pair[0]]    
            score2 = scores[pair[1]]
            if score1 < th or score2 < th:
                continue
            x1 = int(minmax(keypoints[pair[0]][0], 0, ret.shape[1] - 1))
            y1 = int(minmax(keypoints[pair[0]][1], 0, ret.shape[0] - 1))
            x2 = int(minmax(keypoints[pair[1]][0], 0, ret.shape[1] - 1))
            y2 = int(minmax(keypoints[pair[1]][1], 0, ret.shape[0] - 1))

            print(y1, x1, y2, x2)
            rr, cc = line(y1, x1, y2, x2)
            _color = np.array(color).astype(np.uint8)
            ret[rr, cc, :] = _color
        
    return ret


def draw_keypoint2video(path_export, path_video, path_json):
    
    cap = cv2.VideoCapture(path_video)
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fmt = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    writer = cv2.VideoWriter(path_export, fmt, fps, (width, height))

    coco = COCO(path_json)
    # it considers only a category ('person')
    skeleton = coco.dataset['categories'][0]['skeleton']
    
    print(skeleton)

    loop_video = 0
    while True:
        
        loop_video += 1
        success, image = cap.read()

        if success:
            
            image_keypoint = image.copy()
            Ids = coco.getAnnIds(imgIds=loop_video)
            labels = coco.loadAnns(Ids)
            
            image_keypoint = cv2.cvtColor(image_keypoint, cv2.COLOR_RGB2BGR)
            image_keypoint = draw_keypoint2img(image_keypoint, labels, skeleton)
            image_keypoint = cv2.cvtColor(image_keypoint, cv2.COLOR_BGR2RGB)
            writer.write(image_keypoint)
            
        else:
            break
        
    cap.release()
    writer.release()
